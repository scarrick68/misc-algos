(function(){
    // There are N integers in an array A. All but one
    // integer occur in pairs. Your task is to find out
    // the number that occurs only once.

    // example input: [0, 0, 1, 2, 1]
    // expected result: 2

    // Test Cases:
    // What if the array is empty?
    // What if there's only one element in the array?

    function onlyOnce(arr){
        var counter = {};
        if(arr.length === 0){
            return undefined
        }
        for(var i = 0; i<arr.length; i++){
            // if the current element in the array doesn't exist in the counter object, add it.
            if(!counter.hasOwnProperty(arr[i])){
                counter[arr[i]] = true;
            }
            else{
                delete counter[arr[i]]
            }
        }
        // parse back to int because obj keys are strings.
        var number = parseInt(Object.keys(counter)[0]);
        return number 
    }

    // Given a string of open or closed parenthesis write a
    // function which will return true if the parenthesis are
    // valid ex '(())()' or false when not valid ex '()()))'

    // Test Cases:
    // Pair of parens
    // Nested parens
    // Sibling sets of parens
    // Empty string
    // Single element string (open or close)
    // Extra open / close
    // Many extra open / close
    function validParens(str, open, close){
        var stack = [];
        var element = '';
        for(var i = 0; i < str.length; i++){
            if(str[i] === open){
                element = stack.push(str[i]);
            }
            else if(str[i] === close){
                element = stack.pop(str[i]);
                if(element === undefined || element === null){
                    return false
                }
            }
            else{
                console.error('invalid character detected. not a parthesis.')
                return false
            }
        }
        if(stack.length > 0){
            return false
        }
        return true
    }


    // Given an array A of N distinct non negitive integers
    // and a positive integer X write a function to find the
    // number of pairs (a,b) (for a and b in A) such that a+b<=X
    // example input: [1,2,3,4,5], x = 3

    // return number of pairs
    // sort the array
    // loop through from the beginning
    // binary search to find index of largest element <= max - arr[i]

    // Time-complexity: O(N*log(N))
    // Space-complexity: O(1) depending on underlying sort algorithm.
    // possible that it is greater.

    // Test Cases:
    //  - empty array
    //  - one element array
    //  - no pair in the list satisfies the condition

    function sumPairs(arr, max){
        arr.sort(function(a,b){return a-b});
        var diff = 0;
        var pairCount = 0;
        var index;
        console.log(arr)
        if(arr.length < 2 || arr[0] > max){
            return pairCount
        }
        for(var i = 0; i < arr.length; i++){
            diff = max - arr[i];
            if(diff < 0){
                return pairCount
            }
            index = binSearch(arr, diff);
            if((index - i) < 0){
                return pairCount
            }
            pairCount = pairCount + (index - i);
            console.log('data', arr[i], i, index, pairCount)
        }

        return pairCount
    }

    // Just to initialize start and end values.
    // Could be written without this.
    function binSearch(arr, val){
        if(arr.length === 0){
            return undefined
        }
        var index = doBinSearch(arr, val, 0, arr.length-1);
        return index
    }

    // look for the maximum value <= to val
    // return the index
    // Does it reach the lowest value? -> yes.
    // Does it reach the highest value? -> yes.
    // What if ALL values are too large? -> return false.
    // That case is actually handled earlier in the sumPairs function.
    // Left it here as well for possible future use.
    function doBinSearch(arr, val, start, end){
        var index = -1
        var mid = Math.ceil((start + end) / 2)
        var midVal = arr[mid]
        if(arr[mid] === val){
            return mid
        }
        // such a value cannot be found in this data set.
        if(arr[0] > val){
            return false
        }
        else if(arr[mid] < val){
            // did not find the element
            if((start === mid || end === mid) && arr[mid] !== val){
                return mid
            }
            index = doBinSearch(arr, val, mid+1, end)
        }
        else if(arr[mid] > val){
            index = doBinSearch(arr, val, start, mid-1)
        }

        return index
    }

    // var sortedArr = [1,2,3,4,5,6,7,8,9,10]
    // var incompleteArr = [1,5,10,12,16,17,20,24,28,32,40]
    // var index = binSearch(incompleteArr, 6)
    // console.log(index)

    module.exports = {
        binSearch: binSearch,
        onlyOnce: onlyOnce,
        validParens: validParens,
        sumPairs: sumPairs
    };
})()
