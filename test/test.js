// General Note:
/*
    I think this suite covers a pretty good number of cases.
    Some test cases may be slightly redundant in terms of the
    edge case they test for, but I probably put it in because
    it was an issue while debugging and it was easy enough to
    copy / paste another test in. It's well worth any debugging
    time it saves.
*/

var floqast = require('../floqast-int');
var expect = require('chai').expect;

// used in several tests
var emptyArr = []
var oneVal = [7]

// Binary Search returns the expected index of the value
// or the largest value < that value.
describe('Modded Binary Search', function(){
    describe('Given value exists', function(){
    	var sortedArr = [1,2,3,4,5,6,7,8,9,10]
        it('The value that was passed in exists somewhere in the middle of the array', function(){
    			var index = floqast.binSearch(sortedArr, 8)
    			expect(index).to.equal(7);
        });
        it('The given value exists and is the first element in the array', function(){
           var index = floqast.binSearch(sortedArr, 1)
           expect(index).to.equal(0);
        });
        it('The given value exists and is the last element in the array', function(){
           var index = floqast.binSearch(sortedArr, 10)
           expect(index).to.equal(9);
        });
    });

    describe("Given value doesn't exist", function(){
    	var incompleteArr = [1,5,10,12,16,17,20,24,28,32,40]
        it("Value doesn't exist. Second largest is somewhere in the middle of the array.", function(){
           var index = floqast.binSearch(incompleteArr, 6)
           var value = incompleteArr[index]
           expect(index).to.equal(1);
           expect(value).to.equal(5);
        });
        it("Value doesn't exist. Second largest is first element in the array.", function(){
           var index = floqast.binSearch(incompleteArr, 2)
           var value = incompleteArr[index]
           expect(index).to.equal(0);
           expect(value).to.equal(1); 
        });
        // testing because it came up in debugging.
        it("Value doesn't exist. Second largest is second element in the array.", function(){
           var index = floqast.binSearch(incompleteArr, 6)
           var value = incompleteArr[index]
           expect(index).to.equal(1);
           expect(value).to.equal(5); 
        });
        it("Value doesn't exist. Second largest is last element in the array.", function(){
            var index = floqast.binSearch(incompleteArr, 45)
           var value = incompleteArr[index]
           expect(index).to.equal(incompleteArr.length-1);
           expect(value).to.equal(40);
        });
        it("Value doesn't exist. Second largest is second to last element in the array.", function(){
            var index = floqast.binSearch(incompleteArr, 39)
           var value = incompleteArr[index]
           expect(index).to.equal(incompleteArr.length-2);
           expect(value).to.equal(32);
        });
    });
});

describe('Test Suite For Lone Integer Algorithm', function(){
  var pairsArr = [1,2,3,4,5,4,3,2,1]
  it('Return lone integer', function(){
    var loneInt = floqast.onlyOnce(pairsArr)
    expect(loneInt).to.equal(5);
  });
  it('Return lone int from single val array', function(){
    var loneInt = floqast.onlyOnce(oneVal)
    expect(loneInt).to.equal(7);
  });
  it('Return lone int from empty arr. Expecting undefined.', function(){
    var loneInt = floqast.onlyOnce(emptyArr)
    expect(loneInt).to.equal(undefined);
 });
});

describe('Test Suite For Valid Parenthesis Algorithm', function(){
  it('Valid string', function(){
    var valid = floqast.validParens('()', '(', ')')
    expect(valid).to.equal(true);
  });
  it('Valid nested string', function(){
    var valid = floqast.validParens('((()))', '(', ')')
    expect(valid).to.equal(true);
  });
  it('Valid sibling string', function(){
    var valid = floqast.validParens('()()()', '(', ')')
    expect(valid).to.equal(true);
  });
  it('Valid sibling and nested string', function(){
    var valid = floqast.validParens('()(())', '(', ')')
    expect(valid).to.equal(true);
  });
  // Returning true for this. number ( === number ) === 0. 
  it('Valid empty string', function(){
    var valid = floqast.validParens('', '(', ')')
    expect(valid).to.equal(true);
  });
  it('Invalid single open paren', function(){
    var valid = floqast.validParens('(', '(', ')')
    expect(valid).to.equal(false);
  });
  it('Invalid single closing paren', function(){
    var valid = floqast.validParens(')', '(', ')')
    expect(valid).to.equal(false);
  });
  it('Invalid extra open paren', function(){
    var valid = floqast.validParens('(()', '(', ')')
    expect(valid).to.equal(false);
  });
  it('Invalid extra closing paren', function(){
    var valid = floqast.validParens('(()))', '(', ')')
    expect(valid).to.equal(false);
  });
  it('Valid brackets', function(){
    var valid = floqast.validParens('{{}}', '{', '}')
    expect(valid).to.equal(true);
  });
  it('Invalid brackets', function(){
    var valid = floqast.validParens('{}}', '{', '}')
    expect(valid).to.equal(false);
  });
  it('Valid square brackets', function(){
    var valid = floqast.validParens('[[]]', '[', ']')
    expect(valid).to.equal(true);
  });
  it('Invalid square brackets', function(){
    var valid = floqast.validParens('[]]', '[', ']')
    expect(valid).to.equal(false);
  });
});

describe('Test Suite For Sum of Pairs Algorithm', function(){
  it('Sum of pairs 1', function(){
    var pairs = floqast.sumPairs([2,1,4,3], 7)
    expect(pairs).to.equal(6);
  });
  it('Sum of pairs 2', function(){
    var pairs = floqast.sumPairs([2,1,4,3,6,5,9,8,7], 10)
    expect(pairs).to.equal(20);
  });
  it('Sum of pairs 3', function(){
    var pairs = floqast.sumPairs([2,1,4,3,6,5,9,8,7], 5)
    expect(pairs).to.equal(4);
  });
  // 7 satisfies the condition <= 10, but there's no pair so returning 0 pairs.
  it('One element array', function(){
    var pairs = floqast.sumPairs(oneVal, 10)
    expect(pairs).to.equal(0);
  });
  it('Empty array', function(){
    var pairs = floqast.sumPairs(emptyArr, 10)
    expect(pairs).to.equal(0);
  });
  it('All pairs >= maxVal', function(){
    var pairs = floqast.sumPairs([11,13,12,17,14,18,21,20], 10)
    expect(pairs).to.equal(0);
  });
});